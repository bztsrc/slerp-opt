/*
 * slerp_cross.c
 *
 * Copyright (C) 2019 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief cross-platform optimized SLERP implementation
 * https://gitlab.com/bztsrc/slerp-opt
 *
 * Compile with:
 *   cc slerp_cross.c -o slerp -lm
 *
 */

#include <math.h>

/**
 * The SLERP function itself
 */
void slerp(float *qa, float *qb, float t, float *ret) {
    float a = 1.0 -t, b = t, d = qa[0] * qb[0] + qa[1] * qb[1] + qa[2] * qb[2] + qa[3] * qb[3], c = fabsf(d);
    if(c < 0.999) {
        c = acosf(c);
        b = 1 / sinf(c);
        a = sinf(a * c) * b;
        b *= sinf(t * c);
        if(d < 0) b = -b;
    }
    ret[0] = qa[0] * a + qb[0] * b;
    ret[1] = qa[1] * a + qb[1] * b;
    ret[2] = qa[2] * a + qb[2] * b;
    ret[3] = qa[3] * a + qb[3] * b;
}

/**
 * Example invocation
 */

/* mimic a C++ class */
typedef struct {
    float w;
    float x;
    float y;
    float z;
    void *virtualfunc1;
    void *virtualfunc2;
} quat;

int main(int argc, char **argv)
{
    /* define our quaternion "objects" */
    quat qa, qb, ret;

    /* convert high level class struct into a float array by casting */
    slerp((float*)&qa, (float*)&qb, 0.75, (float*)&ret);
}
